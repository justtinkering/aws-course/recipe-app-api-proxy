# Recipe App API Proxy

NGINX proxy app for our recipe app API

[![pipeline status](https://gitlab.com/justtinkering/aws-course/recipe-app-api-proxy/badges/master/pipeline.svg)](https://gitlab.com/justtinkering/aws-course/recipe-app-api-proxy/-/commits/master)
[![coverage report](https://gitlab.com/justtinkering/aws-course/recipe-app-api-proxy/badges/master/coverage.svg)](https://gitlab.com/justtinkering/aws-course/recipe-app-api-proxy/-/commits/master)

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)
